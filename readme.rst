###################
CodeIgniter Updater
###################

A small and lightweight way of checking the version of your CodeIgniter install. If the versions don't match, it will download the latest version from Github, and update your install with the latest files. This is designed for people who want a GUI version, and not having a Composer version.

Built as a project for myself (as part of something else I am working on), this is ideal if you want to include within an application.

Currently running on the following libraries:

- <http://phphttpclient.com/> - Currently using the PHP Phar version

*******************
Notes:
*******************

Not endorsed by the CodeIgniter team. This has been purely built as a personal project.

USE AT OWN RISK.

***************
Acknowledgement
***************

Thank you to the CodeIgniter team for building a wonderful framework, and also to you ... the user who took the time to read all of this.
