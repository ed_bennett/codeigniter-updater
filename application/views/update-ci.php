<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="inc/_css/master.css">
</head>
<body>

	<h1>Update your CodeIgniter</h1>

	<button class="button">Check version</button>

	<div class="message">
		<p class="status"></p>
		<p class="current"></p>
		<p class="latest"></p>
		<a href="<?php echo base_url(); ?>">Return to your project</a>
	</div>

	<footer>
		<a href="http://edthewebdev.pro">Crafted with <i class="fa fa-heart"></i> by Ed</a>
		<p>Not affiliated with the CodeIgniter team. Built purely as a personal project.</p>
		<p>Background generated at <a href="http://www.heropatterns.com/" target="_blank">Hero Patterns</a> under the <a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank">CC BY-SA 4.0 License</a></p>
	</footer>



	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.button').on('click', function(e) {
				$(this).html('<i class="fa fa-cog fa-spin fa-fw fa-lg"></i>').addClass('wait');
				e.preventDefault();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url() . 'update/check_version'; ?>",
					dataType: 'json',
					success: function(data) {
						setTimeout(function() {
							if(data.success == 'Everything is up to date!') {
								$('div.message').addClass('success');
								$('.button').attr('disabled', 'disabled').removeClass('wait');
								$('p.status').html('<i class="fa fa-check fa-lg"></i> ' + data.success);
								$('p.current').html('Your current CI Version is: ' + data.your_version);
								$('p.latest').html('The latest version of CI on Github is: ' + data.latest_version_github);
							} else {
								console.log(data);
								$('.button').html('<i class="fa fa fa-exclamation-triangle fa-lg"></i> ' + data.update_required).addClass('warning').removeClass('wait');
								$('.message').addClass('warning');
								$('p.current').html('Your current CI Version is: ' + data.your_version);
								$('p.latest').html('The latest version of CI on Github is: ' + data.latest_version_github);

								downloadLatestVersion();
							}


						}, 1000);
					}
				})
			})

			function downloadLatestVersion() {
				console.log('Hello');
			}
		});
	</script>
</body>
</html>
