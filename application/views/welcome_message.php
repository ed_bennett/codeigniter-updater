<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>YourMedia.info</title>

	<link rel="stylesheet" href="<?php echo base_url(); ?>inc/_css/master.css" />
</head>
<body>

<div id="container">
	<h1>YourMedia.info</h1>

	<p>Site under construction.</p>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds.</p>
	<p class="footer">&copy; <?php echo date('Y'); ?> <a href="http://edthewebdev.pro">edthewebdev.pro</a></p>
	<p class="footer"><a href="mailto:iam@edthewebdev.pro">Talk to me! Available for freelance!</a></p>
</div>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-96244351-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
