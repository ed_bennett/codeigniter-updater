<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('httpful.phar');

class Update extends CI_Controller {

	protected $get_tags;
	protected $get_master;
	protected $ci_versions;
	protected $site_version;
	protected $latest_version;

	public function __construct() {
		parent::__construct();

		// Fetch all the tags from the CodeIgniter Repo
		$this->get_tags = \Httpful\Request::get('https://api.github.com/repos/bcit-ci/CodeIgniter/tags')->send();

		// Now set up an empty Array
		$this->ci_versions = array();

		// Decode all the content from the get request
		$decode = json_decode($this->get_tags, true);

		// Enter each of the release tags into the versions array
		foreach($decode as $version) {
			array_push($this->ci_versions, $version['name']);
		}

		// Sort out all the versions into order
		usort($this->ci_versions, 'version_compare');

		// Set up the versions for the site
		// TO DO: Make these into protected variables above
		$this->site_version = 3;
		$this->latest_version = end($this->ci_versions);

		// Now fetch a master url (TO DO: rename this to better variable. Not a master)
		$this->get_master = \Httpful\Request::get('https://api.github.com/repos/bcit-ci/CodeIgniter/zipball/' . $this->latest_version)
		->send();
	}

	// Default display for the updater
	public function index() {
		$this->load->view('update-ci');
	}

	// Check your installation versus the latest on Github
	public function check_version() {
		// If we match we output some JSON to the browser, that is grabbed by an ajax request
		if( $this->site_version == $this->latest_version ) :
			$data = array('success' => 'Everything is up to date!', 'latest_version_github' => $this->latest_version, 'your_version' => $this->site_version);
			header('Content-Type: application/json');
			$output = json_encode($data);
			die($output);
		else:
			$data = array('update_required' => 'Update available', 'latest_version_github' => $this->latest_version, 'your_version' => $this->site_version);
			header('Content-Type: application/json');
			$output = json_encode($data);
			die($output);
		endif;
	}

	// Lets download the latest version from Github
	public function download_latest_version() {

		// Set the document root.
		$root = $_SERVER['DOCUMENT_ROOT'];

		// Name of the file to generate if it doesn't exist
		$zip = '/master.' . $this->latest_version . '.zip';

		// Full path of file
		$full_path = $root . $zip;

		// Check for existing file. Create the file if it doesn't.
		if( !file_exists( $full_path ) ) :

			// Check for the cURL library on the server. Die if it doesn't exist.
			if ( ! function_exists( 'curl_init' ) ) die( 'The cURL library is not installed.' );

			// Create the file at the path defined earlier.
			$file = fopen( $full_path, 'w+' );

			// Using cURL we download the zip file from Github and put it into the file we created
			$curl = curl_init();
			curl_setopt( $curl, CURLOPT_URL, $this->get_master->meta_data['redirect_url']);
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $curl, CURLOPT_FILE, $file );

			// Execute the above commands
			$result = curl_exec( $curl );

			// Close everything off
			curl_close( $curl );
			fclose( $file );

			// Create the data for the ajax request.
			$data = array('cached' => false, 'message' => 'Download complete.');
			header('Content-Type: application/json');
			$output = json_encode($data);
			die($output);

		else:

			// Otherwise, the file does exist already. We will then use this version to update.
			// This feature prevents multiple requests to the Github API within 1 hour of the file being created.

			// The current time
			$current_time = time();

			// The time we want the file to expire.
			$expire_time = strtotime('+1 hour');

			// Get the time stamp on the file we created.
			$file_time = filemtime($full_path);

			// Now we double check the file definitely exists, and we compare the time stamps.
			// If the file is less than an hour old, simply output the data for the ajax request,
			if( file_exists( $full_path ) && ( $current_time - $expire_time < $file_time ) ) :

				$data = array('cached' => true, 'message' => 'Cached download found. Using this to update.');
				header('Content-Type: application/json');
				$output = json_encode($data);
				die($output);

			endif;

		endif;
	}
}


?>
